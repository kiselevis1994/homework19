#include <iostream>
#include <string>
using namespace std;


class Animal 
{
	
public:
	
	string MyName;
//	Animal() {}
	Animal(string str) 
	{
		MyName = str;
	}

	virtual void voice() 
	{
		cout << "� ����� ������������� ������" << endl;
	}
};

class Dog : public Animal
{
public:

	Dog(string _a): Animal(_a)
	{
		
	}

	void voice() override
	{
		cout << "� " << Animal::MyName << " � ������: ���-���"<<endl;
	}

};

class Cat : public Animal 
{
	
public:
	Cat(string _a): Animal(_a)
	{

	}
	void voice() override
	{
		cout << "� " << Animal::MyName << " � ������: ����" << endl;
	}
};

class Cow : public Animal 
{
public:

	Cow(string _a): Animal(_a)
	{
	
	}

	void voice() override
	{
		cout <<"� "<<Animal::MyName<<" � � ������: ��-��" << endl;
	}
};


void main()
{
	setlocale(LC_ALL, "Russian");
	
	Cat cat1("�����");
	Dog dog1("������");
	Cow cow1("������");
	Animal *ArrayOfAnimals[3];
	ArrayOfAnimals[0] = &cat1;
	ArrayOfAnimals[1] = &dog1;
	ArrayOfAnimals[2] = &cow1;
	for (int i = 0; i < 3; i++) 
	{
		ArrayOfAnimals[i]->voice();
	}
}